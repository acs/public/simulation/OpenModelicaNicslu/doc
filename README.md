# README

## Git clone, setup, and build
Clone the official OpenModelica repository and checkout the version that was used for NICSLU integration
```sh
git clone https://github.com/OpenModelica/OpenModelica
cd OpenModelica
git checkout 37e0d80f7d50cf3eaa4192fb8a8702565c21975d
```
In `.gitmodules` change the settings of submodule `OMC` to the following
```
[submodule "OMC"]
        path = OMCompiler
        url = https://git.rwth-aachen.de/acs/core/OpenModelicaNicslu/OMCompiler.git
```
Update all submodules and change to the nicslu branches:
```sh
git submodule update --init
cd OMCompiler/
git checkout nicslu
git submodule update --init
cd ..
git submodule update --init --recursive
cd OMCompiler/
git checkout nicslu
cd 3rdParty/
git checkout nicslu
cd ../..
```
Build the OM compiler and all OM libraries:
```sh
autoconf
./configure --with-omniORB=/usr CC=clang CXX=clang++
make -j 16 omc
make -j 16 omlibrary-all
```
